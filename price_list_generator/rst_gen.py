#!/usr/bin/env python3
#  -*- coding: utf-8 -*-
#  # @author: Arnaud Joset
#
#  This file is part of Price_list_generator.
#
# Price_list_generator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Price_list_generator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Price_list_generator.  If not, see <http://www.gnu.org/licenses/>.
#

from . import constants as c
from . import rst_tools
import logging
import pandas as pd

class Rst:
    def __init__(self):
        self.logger = self.logger = logging.getLogger(__name__)
        self.price_list_template_tex = 'rst/price_list_template.rst'
        self.prices_tex = 'rst/prices.rst'
        self.prices = {'ERROR': '', 'tmp': '', 'rouge': '', 'blanc': '', 'no': '', 'rosé': '',
                       'ambré': '', 'grenat': '', 'bulles': ''}
        # self.table_line = "|{code} | {name} | {vintage} | {note} | {price}|\n"
        # https://stackoverflow.com/questions/11347505/what-are-some-approaches-to-outputting-a-python-data-structure-to-restructuredte
        # create tables
        # self.table_bioline = "|{code} | {name} | {vintage} | {note} | {price}|\n"
        # self.noteline = "\\multicolumn{{5}}{{p{{16.5cm}}}}{{\\noteline{{{d}}}}}\\\\\n"
        self.table = []
        self.total_wines = 0
        self.wine_counter = 0
        for v in c.ORDERED_REGIONS:
            self.prices[v] = ''
        self.added_wines = []
        self.codes = []

    def gen_section(self, section_str, sectype='title'):
        size = len(section_str)
        equals = '=' * size
        minus = '-' * size
        circon = '^' * size
        my_title = ''
        if sectype == 'title':
            my_title = equals + '\n' + section_str + '\n' + equals + '\n'
        if sectype == 'subsection':
            my_title = section_str + '\n' + equals + '\n'
        if sectype == 'subsubsection':
            my_title = section_str + minus + '\n'
        if sectype == 'subsubsubsection':
            my_title = section_str + '\n' + circon + '\n'
        return my_title

    def gen_templates(self):
        # 5 columns
        line = "+{}+{}+{}+{}+{}+\n".format('-' * 10, '-' * 50, '-' * 50, '-' * 40, '-' * 10)
        self.table = line + '--DATA--' + line

    def write_prices(self):

        self.logger.info("Number of wines in xlsx : {}".format(self.total_wines))
        self.logger.info("Number of wines added : {}".format(self.wine_counter))

        self.codes = sorted(self.codes)
        self.added_wines = sorted(self.added_wines)
        difference = list(set(self.codes) - set(self.added_wines))
        self.logger.info("Differences: {}".format(difference))
        self.logger.info("FrameBreak 50 to prevent new paragraph at the end of the page")
        if difference:
            self.prices['ERROR'] += 'Tous les vins ne sont pas ajoutés. Check {}'.format(difference)
        with open(self.prices_tex, 'w') as prices_file:
            txt_tarif = self.prices['ERROR'] + '\n'
            for v in c.ORDERED_REGIONS:
                txt_tarif += self.prices[v]
                txt_tarif += "\n .. NEW REGION::\n\n"
            prices_file.write(txt_tarif)

    def rst2pdf(self):
        self.logger.debug("TODO: launch RST2PDF")

    def generate_wines_list(self, flat_list, seller_dict, parameters_dict, regions_dict):
        need_space_section = 15
        need_space_wine = 15
        sorted_list = self.global_wine_sorter(flat_list)
        # We obtain a list of the different regions
        df = pd.DataFrame(sorted_list)
        self.total_wines = df.shape[0]
        self.wine_counter = 0
        self.codes = df['code'].tolist()
        regions = df.region.unique()
        # TODO: change .tex file depending of options.
        try:
            sauts_sections = True
            fiche_degustation = True
            fiche_degustation_xls = parameters_dict['fiche_degustation']
            sauts_sections_xls = parameters_dict['sauts_sections']
            if not fiche_degustation_xls:
                fiche_degustation = False
            if not sauts_sections_xls:
                sauts_sections = False
        except KeyError:
            fiche_degustation = None
            sauts_sections = None
        clear_page = ''
        self.logger.info("Créer les fiches de dégustation : {}".format(fiche_degustation))
        self.logger.info("Ajouter des sauts de page systématiques : {}".format(sauts_sections))
        if sauts_sections:
            clear_page = 'PageBreak oneColumn'

        first_region = True

        for region in regions:

            if not self.check_presence(region, c.ORDERED_REGIONS):
                self.prices['ERROR'] += 'Erreur nom région {} \\\\\n'.format(region)
            msg = "Wines from region {}".format(region)
            self.logger.debug(msg)
            try:
                # Some regions
                region_full = regions_dict[region]['region_full']
            except KeyError:
                region_full = region
            try:
                # First region: no clear page needed.
                clear_page_tmp = clear_page
                if first_region:
                    clear_page_tmp = ''
                    first_region = False

                # self.prices['tmp'] += '\n {}'.format(clear_page) + '\\section{{{}}}\n'.format(
                #    region_full.replace('_', ' '))
                self.prices['tmp'] += self.gen_section(region_full.replace('_', ' '), sectype='title')
            except KeyError:
                self.prices['ERROR'] += '\nERREUR : {} \n'.format(region)
                print("ERROR REGION {}".format(region))
                continue
            # get list of wines from a region
            wines_region = df[(df['region'] == region)]
            sub_regions_df = wines_region.sub_region.unique()
            sub_regions_ref = None
            try:
                sub_regions_ref = c.ORDERED_SUB_REGIONS[region]
            except KeyError:
                ordered_sub_regions = c.ORDERED_SUB_REGIONS
                self.prices['ERROR'] += "Région mal orthographiée {} {} \\\\\n".format(region,
                                                                                       region_full.replace(' ', '_'))
            try:
                sub_ref = c.ORDERED_SUB_REGIONS[region]
            except KeyError:
                # Temporary test
                # s1 = set(region_full)
                # s2 = set('Test_string')
                self.prices['ERROR'] += "Région mal orthographiée {} {} \\\\\n".format(region,
                                                                                       region.replace(' ', '_'), )
                continue
            for sub_region in sub_regions_ref:
                sub_region_dflist = sub_regions_df.tolist()
                if self.check_presence(sub_region, sub_regions_ref):
                    if not self.check_presence(sub_region, sub_regions_ref):
                        self.prices['ERROR'] += 'Erreur nom sous-région {} \n'.format(sub_region)
                    msg = "Subregions for region {}".format(sub_region)
                    self.logger.debug(msg)
                    # Empty sub_regions_df = NO
                    if sub_region != 'NO':
                        # self.prices['tmp'] += '\n\subsection{{{}}}\n'.format(sub_region)
                        pass
                    # get list of wines from a sub_region
                    wines_sub_region = wines_region[(wines_region['sub_region'] == sub_region)]
                    # Get the list of seller in this region
                    sellers = wines_sub_region.seller_name.unique()
                    for seller in sellers:
                        # get the wines from a given producer
                        msg = "Wines from producer {}".format(seller)
                        self.logger.debug(msg)

                        try:
                            # FIXME BUG if new seller, he has no description and it fails.
                            # FIXME if "fiche de degustation", it fails too even if the description is not used
                            # NEED to produce a description: "Seller name: NO description in red"
                            description = seller_dict[seller]['description'] + '\n'
                            presentation = seller_dict[seller]['presentation'] + '\n'

                            # self.prices['tmp'] += '\\needspace{{{}\\baselineskip}}'.format(need_space_wine)
                            if fiche_degustation:
                                self.prices['tmp'] += '\n' + self.gen_section(self.latex_cleaner(seller),
                                                                              sectype='subsubsection') + '\n'
                            else:
                                self.prices['tmp'] += '\n' + self.gen_section(self.latex_cleaner(description),
                                                                              sectype='subsubsection') + '\n'
                                self.prices['tmp'] += '\n.. class:: desc:: \n\n {}\n.. class:: normal:: \n\n'.format(
                                    presentation) + '\n'
                            if len(presentation) > 2:
                                presentation += ' \\\\ \\bigskip'
                        except KeyError:
                            self.prices['ERROR'] += '\nPas de description pour {} \\\\'.format(
                                self.latex_cleaner(seller))
                            # self.prices['tmp'] += '\n' + \\subsubsection{{{}}}\n'.format(self.latex_cleaner(seller))
                            self.prices['tmp'] += '\n' + self.gen_section(seller, sectype='subsubsection')
                            error_msg = "No description for {}".format(seller)
                            self.logger.error(error_msg)

                        # Get the wines sold by a producer
                        wines_seller = wines_sub_region[(wines_sub_region['seller_name'] == seller)]

                        # Get the list of color in this collection
                        colors = wines_seller.color.unique()

                        for color in colors:
                            # get the wines of a given color
                            msg = "Wines of color {}".format(color)
                            self.logger.debug(msg)
                            # Empty  color = NO
                            wines_tab = wines_seller[(wines_seller['color'] == color)]
                            # local sort here to apply custom filter
                            order_content = list(wines_tab.ordre.unique())
                            if order_content != [0]:
                                wines_tab = self.order_wine_sorter(wines_tab)
                            if color != 'NO':
                                if wines_tab.shape[0] < 2:
                                    color_msg = "Le {}".format(color)
                                else:
                                    color_msg = "Les {}s".format(color)
                                if "bulles" in color.lower():
                                    color_msg = 'Les bulles'
                                try:
                                    self.prices[color.lower()] += '\n' + self.gen_section(color_msg,
                                                                                          sectype="subsubsubsection")
                                except KeyError:
                                    self.logger.error("Error couleur inconnue {}".format(color))
                                    self.prices['ERROR'] += "Error couleur inconnue {}".format(color)
                            table = []
                            for index, wine in wines_tab.iterrows():
                                code = wine['code']
                                description = wine['description']
                                description = self.latex_cleaner(description)
                                name = wine['name']  # .replace("&", "\\&").replace("%", '\\%')
                                name = self.latex_cleaner(name)
                                price = wine['price']
                                vintage = wine['vintage']
                                note = wine['note']  # .replace("&", "\\&").replace("%", '\\%')
                                note = self.latex_cleaner(note)
                                # note = note.replace("Nouveauté", "")

                                if 'NM' in code:
                                    vintage = ''
                                if not 'Ne pas reprendre au tarif---'.lower() in note.lower():
                                    note = note.replace('recyclé', '')

                                    if "bio" not in note.lower():
                                        # line = self.table_line.format(code=code, name=name, vintage=vintage, note=note,
                                        #                              price=price)
                                        line = [code, name, str(vintage), note, str(price)]
                                        table.append(line)
                                    else:
                                        note = note.replace("BIO", "")
                                        # line = self.table_bioline.format(code=code, name=name, vintage=vintage,
                                        #                                 note=note, price=price)
                                        line = [code, name, str(vintage), note, str(price)]
                                        table.append(line)
                                    if 'NO' not in description:
                                        # line += self.noteline.format(d=description)
                                        # description = split_long_str('', description, 120)
                                        line = [description, ]
                                        table.append(line)
                                    # table.append(line)
                                self.wine_counter += 1
                                self.added_wines.append(code)

                            # table = self.table.replace('--DATA--', data_tab)
                            # line_tmp = ['P19/H12', 'Mon bon vin super', '2042', 'un commentaire sympa', '15.15']
                            # table = [line_tmp, line_tmp, line_tmp, line_tmp,line_tmp]
                            table_str = rst_tools.make_table(table)
                            # tablefmt = grid, rst, fancy_grid
                            # table_str = tabulate(table, tablefmt="grid")
                            # expression = "| NOPE"
                            # expression_len = len(expression)
                            # table_str = table_str.replace(expression, ' '*expression_len)
                            try:
                                self.prices[color.lower()] += '\n' + table_str + '\n' + '\n'
                            except KeyError:
                                self.logger.error("Error couleur inconnue {}".format(color))
                        self.prices['tmp'] = self.prices['tmp'] + self.prices['blanc'] + self.prices['rosé'] \
                                             + self.prices['rouge'] + self.prices['ambré'] \
                                             + self.prices['grenat'] + self.prices['bulles'] + self.prices['no']
                        self.prices[region] += self.prices['tmp']
                        self.prices['tmp'] = ''
                        self.prices['rouge'] = ''
                        self.prices['blanc'] = ''
                        self.prices['ambré'] = ''
                        self.prices['grenat'] = ''
                        self.prices['rosé'] = ''
                        self.prices['no'] = ''
                        self.prices['bulles'] = ''

                        # self.prices += line.
                else:
                    self.prices['ERROR'] += 'Sous-région {} non trouvée dans {} pour région {}!\\\\ \n'.format(
                        sub_region, sub_regions_df, region)
                    # print(sub_region == sub_regions_df[0])

            # region = ''

        return flat_list

    def global_wine_sorter(self, list_of_wines):
        name_key = 'seller_name'
        color_key = 'color'
        price_key = 'price'
        region_key = 'region'
        code_key = 'code'
        sub_region_key = 'sub_region'
        # From here: https://stackoverflow.com/questions/1143671/python-sorting-list-of-dictionaries-by-multiple-keys
        # reverse with  -d
        newlist = sorted(list_of_wines,
                         key=lambda d: (d[region_key], d[sub_region_key], d[name_key], d[color_key], d[code_key]))
        # print(newlist)
        return newlist

    def order_wine_sorter(self, list_of_wines):
        order_key = 'ordre'
        # From here: https://stackoverflow.com/questions/1143671/python-sorting-list-of-dictionaries-by-multiple-keys
        # reverse with  -d
        df = list_of_wines.sort_values(by=[order_key], ascending=True)
        # print(newlist)
        return df

    def research_wines(self, d=[], keys=[], data=[]):
        df = pd.DataFrame(d)
        query = df[(df[keys[0]] == data[0]) & (df[keys[1]] == data[1])]
        return query

    def order_list_old(self, list_data, model):
        return_list = []
        for el in model:
            for l in list_data:
                if el.lower() in l.lower():
                    return_list.append(l)

        return return_list

    def check_presence(self, item, list_data):
        ret = False
        for x in list_data:
            if x == item:
                ret = True
                break
        a = 0
        return ret

    def latex_cleaner(self, bad_string):
        good_string = bad_string.replace("&", "\\&").replace("%", '\\%').replace('\n', '').replace('\r',
                                                                                                   '')  # .replace('oe','\\oe{}' )
        good_string = bad_string.replace('\\saut', '')
        good_string = good_string.replace('œ', 'oe')
        return good_string


def split_long_str(good_str, my_str, n_cut):
    """
    Cut long lines after n_cut caracter. Find the position of the nearest space at n_cut position and
    insert an \n character. The function call herself recoursively to cut each segment longueur than cut.
    """
    if len(my_str) > n_cut:
        pos = my_str.find(' ', n_cut)
        good_str += my_str[:pos] + '\n'
        tail = my_str[pos:]
        return split_long_str(good_str, tail, n_cut)
    else:
        good_str += my_str
        return good_str


if __name__ == '__main__':
    my_str = """C'est une super longue ligne ha ha ha. Délicieux bouquet floral et épicé, serti de tanins fermes 
    en filigrane. Ce rouge se révèlera davantage dans cinq ans." (RVF juin 2018) Mais vraiment longues !!!"""
    str_cut, _ = split_long_str("", my_str, 10)
