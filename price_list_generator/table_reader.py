#!/usr/bin/env python3
#  -*- coding: utf-8 -*-
#  # @author: Arnaud Joset
#
#  This file is part of Price_list_generator.
#
# Price_list_generator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Price_list_generator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Price_list_generator.  If not, see <http://www.gnu.org/licenses/>.
#

import logging
import xlrd
from . import constants as c


class Table:
    def __init__(self, ):
        self.logger = logging.getLogger(__name__)
        self.parameters = {}

    def prepare_lists(self):
        tarif = {}
        for r in c.ORDERED_REGIONS:
            tarif[r] = []
        self.logger.debug(tarif)
        tarif['ERROR'] = []
        return tarif

    def find_region(self, region):
        # If the region has mistake, it will be shown here
        correct_region = c.PROBLEM + ' : ' + region # + ' ' + region.replace(" ", '\_')
        region = region.replace('–', '-')
        # remove errors like 'Bourgogne '
        region = region.strip()
        # Fix problems in Maconnais Beaujolais
        beaujolais_maconnais_string = ('beaujolais', 'maconnais')
        # for bmstring in beaujolais_maconnais_string:
        #     if bmstring in region.lower():
        #         region = 'Beaujolais - Maconnais'
        for r in c.ORDERED_REGIONS:
            # remove _ in Rhône_Nord
            real_r = r.replace('_', ' ')
            if region.lower() in real_r.lower():
                correct_region = r
                break
        self.logger.debug(correct_region)
        return correct_region

    def wines_reader(self, parameters={}):
        error_nbr = 0
        log_msg = ''
        self.parameters = parameters
        filename = parameters['price_list']
        self.logger.debug(filename)
        price_list = self.prepare_lists()
        flat_list = []
        wb = xlrd.open_workbook(filename)
        self.logger.debug(wb.sheet_names())
        sh = wb.sheet_by_name(wb.sheet_names()[0])
        for rownum in range(1, sh.nrows):
            try:
                data = {}
                try:
                    data['vintage'] = int(sh.row_values(rownum)[3])
                except ValueError:
                    data['vintage'] = 0
                    pass
                data['code'] = sh.row_values(rownum)[0]
                data['name'] = sh.row_values(rownum)[1]
                color = sh.row_values(rownum)[2]
                color = color.strip()
                data['color'] = color

                data['seller_name'] = sh.row_values(rownum)[4]
                data['region'] = sh.row_values(rownum)[5]
                data['sub_region'] = sh.row_values(rownum)[6]
                if not data['sub_region']:
                    data['sub_region'] = "NO"
                if not data['color']:
                    data['color'] = 'NO'
                data['price'] = sh.row_values(rownum)[7]
                data['standard_price'] = sh.row_values(rownum)[8]
                data['note'] = sh.row_values(rownum)[9]
                try:
                    order = sh.row_values(rownum)[10]
                    if order == '':
                        order = 0
                    data['ordre'] = float(order)
                except ValueError:
                    data['ordre'] = 0
                    msg = " {} LA COLONNE ORDRE NE DOIT CONTENIR QUE DES NOMBRES !!!".format(data['code'])
                    log_msg += msg + '\n'
                    error_nbr += 1
                    self.logger.error(msg)
                if not data['ordre']:
                    data['ordre'] = 0
                data['description'] = sh.row_values(rownum)[11]
                if not data['description']:
                    data['description'] = "NO"
                data['region'] = self.find_region(data['region'])

                try:
                    price_list[data['region']].append(data)
                except KeyError:
                    price_list['ERROR'].append(data)
                flat_list.append(data)
            except IndexError:
                msg = "CRITICAL: EXCEL FILE: bad number of columns"
                log_msg += msg + '\n'
                error_nbr += 1
                logging.critical(msg)
                return None, None, log_msg, error_nbr

        # self.logger.debug(price_list)
        # print(price_list)
        return price_list, flat_list, log_msg, error_nbr

    def seller_reader(self, filename=''):
        self.logger.debug(filename)
        sellers_dict = {}
        wb = xlrd.open_workbook(filename)
        self.logger.debug(wb.sheet_names())
        sh = wb.sheet_by_name(wb.sheet_names()[1])
        for rownum in range(1, sh.nrows):
            seller = sh.row_values(rownum)[0]
            description = sh.row_values(rownum)[1]
            presentation = sh.row_values(rownum)[2]

            seller = clean_text(seller, self.parameters)
            description = clean_text(description, self.parameters)
            presentation = clean_text(presentation, self.parameters)
            sellers_dict[sh.row_values(rownum)[0]] = {'seller': seller,
                                                      'description': description,
                                                      'presentation': presentation}
        return sellers_dict

    def regions_reader(self, filename=''):
        self.logger.debug(filename)
        regions_dict = {}
        wb = xlrd.open_workbook(filename)
        self.logger.debug(wb.sheet_names())
        sh = wb.sheet_by_name(wb.sheet_names()[3])
        for rownum in range(1, sh.nrows):
            region = sh.row_values(rownum)[0]
            region_full = sh.row_values(rownum)[1]

            region = clean_text(region, self.parameters)
            region_full = clean_text(region_full, self.parameters)
            regions_dict[region] = {'region': region,
                                    'region_full': region_full}
        return regions_dict

def clean_text(msg, parameters):
    if parameters['type'].lower() == 'latex':
        msg = str(msg)
        msg = msg.replace("*", '$\\bigstar$').replace("-", "-").replace('\u0092', "'").replace('\u0153',
                                                                                               "\\oe{}").replace(
            '\u009c', '')
        # \u0092 est ' mais excel le met en double
        msg = msg.replace("''", "'")
        # .replace('\u0092', "")
        # msg = msg.replace('œ', 'oe')
    else:
        msg = msg.replace("*", '\*')

    return msg
