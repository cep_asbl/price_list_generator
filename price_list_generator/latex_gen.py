#!/usr/bin/env python3
#  -*- coding: utf-8 -*-
#  # @author: Arnaud Joset
#
#  This file is part of Price_list_generator.
#
# Price_list_generator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Price_list_generator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Price_list_generator.  If not, see <http://www.gnu.org/licenses/>.
#
import logging
import pandas as pd
import os
from . import constants as c


class Latex:
    def __init__(self, price_list_template_tex='latex/price_list_template.tex',
                 tabular_template_tex='latex/tabular_template.tex', prices_tex='latex/prices.tex'):
        self.logger = logging.getLogger(__name__)
        self.price_list_template_tex = price_list_template_tex
        self.tabular_template_tex = tabular_template_tex
        self.prices_tex = prices_tex
        self.prices = {'ERROR': '', 'tmp': '', 'rouge': '', 'blanc': '', 'no': '', 'rosé': '',
                       'ambré': '', 'grenat': '', 'bulles': '', 'orange': ''}
        self.table_line = "{code} & {name} & {vintage} & {note} & {price}\\\\\n"
        self.table_bioline = "\\bio{{{code}}} & \\bio{{{name}}} & \\bio{{{vintage}}} & \\bio{{{note}}} & \\color{{ForestGreen}} {price} \\\\\n"
        self.noteline = "\\multicolumn{{5}}{{p{{16.5cm}}}}{{\\noteline{{{d}}}}}\\\\\n"
        self.table = ''
        self.total_wines = 0
        self.wine_counter = 0
        for v in c.ORDERED_REGIONS:
            self.prices[v] = ''
        self.added_wines = []
        self.codes = []
        self.breaks_list = []
        self.breaks = None

    def gen_templates(self):
        try:
            with open(self.tabular_template_tex, 'r', encoding='utf-8') as price_file:
                self.table = price_file.read()
                return None
            # price_list_string.replace('--DATA--', data_tab)
        except FileNotFoundError as not_found:
            self.table = None
            filename = get_file_with_parents(not_found.filename)
            msg = "File not found : {}".format(filename)
            return msg

    def write_prices(self):
        error_nbr = 0
        msg = "Number of wines in xlsx : {}".format(self.total_wines)
        self.logger.info(msg)
        msg = "Nombre de vins dans le fichier xlsx : {}".format(self.total_wines)
        log_msg = msg + '\n'
        msg = "Number of wines added : {}".format(self.wine_counter)
        self.logger.info(msg)
        msg = "Nombre de vins ajoutés: {}".format(self.wine_counter)
        log_msg += msg + '\n'
        self.codes = sorted(self.codes)
        self.added_wines = sorted(self.added_wines)
        difference = list(set(self.codes) - set(self.added_wines))
        msg = "Differences: {}".format(difference)
        self.logger.info(msg)
        log_msg += msg + '\n'
        if difference:
            msg = 'Tous les vins ne sont pas ajoutés. Check {}'.format(difference)
            self.prices['ERROR'] += msg
            log_msg += msg + '\n'
            error_nbr += 1
        with open(self.prices_tex, 'w', encoding='utf-8') as prices_file:
            txt_tarif = self.prices['ERROR'] + '\n'
            for v in c.ORDERED_REGIONS:
                txt_tarif += self.prices[v]
            prices_file.write(txt_tarif)
        self.logger.info(f"Tarif écrit dans fichier {self.prices_tex}")
        return log_msg, error_nbr

    def pdflatex(self):
        self.logger.debug("TODO: launch PDFLATEX")

    def debug(self, idx):
        if self.debug_breaks:
            return "\#" + str(idx) + ' '
        else:
            return ""

    def page(self, idx):
        if self.breaks != 'manual':
            # breaks: always or never
            return self.clear_page
        else:
            if idx in self.breaks_list:
                # manual: only clearpage for defined idx
                return '\\clearpage'
            else:
                return ''

    def generate_wines_list(self, flat_list, seller_dict, parameters_dict, regions_dict):
        log_msg = ''
        error_nbr = 0
        need_space_section = 15
        need_space_wine = 15
        sorted_list = self.global_wine_sorter(flat_list)
        # We obtain a list of the different regions
        df = pd.DataFrame(sorted_list)
        self.total_wines = df.shape[0]
        self.wine_counter = 0
        self.codes = df['code'].tolist()
        regions = df.region.unique()
        # TODO: change .tex file depending of options.
        try:
            tasting_notes = parameters_dict['tasting_notes']
            self.breaks = parameters_dict['breaks']
        except KeyError:
            tasting_notes = None
            page_breaks = None
        clear_page = ''
        nice_tasting = lambda tasting_notes: "Oui" if tasting_notes else "Non"
        breaks_options = {'always': 'Toujours', 'manual': 'Manuels', 'never': 'Jamais'}
        msg = "Créer les fiches de dégustation : " + nice_tasting(tasting_notes)
        self.logger.info(msg)
        log_msg += msg + '\n'
        msg = "Ajouter des sauts de page : " + breaks_options[self.breaks]
        self.logger.info(msg)
        log_msg += msg + '\n'
        # FIXME breaks
        first_region = True
        self.breaks_list = parameters_dict['breaks_list']
        # TODO: if idx in breaks_list --> add clear_page
        self.debug_breaks = parameters_dict['debug_breaks']
        idx = 0
        for region in regions:
            self.prices['tmp'] = '' + '\\needspace{{{}\\baselineskip}}'.format(need_space_section)
            if not self.check_presence(region, c.ORDERED_REGIONS):
                self.prices['ERROR'] += '\\noindent Erreur nom région {} \\\\\n'.format(region)
            msg = "Wines from region {}".format(region)
            self.logger.debug(msg)
            # log_msg += msg + '\n'
            try:
                # Some regions
                region_full = regions_dict[region]['region_full']
            except KeyError:
                region_full = region
            if self.debug_breaks:
                region_full = region_full
            try:
                # First region: no clear page needed.
                if first_region:
                    clear_page = ''
                    first_region = False
                else:
                    if self.breaks in ['always', 'manual']:
                        clear_page = '\\clearpage'
                    else:
                        # never
                        clear_page = ''
                self.clear_page = clear_page

                self.prices[
                    'tmp'] += f"\n {self.page(idx)}" + f"\\section{{{self.debug(idx)}{region_full.replace('_', ' ')}}}\n"
                idx += 1
            except KeyError:
                # Error in region name (region mal orthograhiée)
                self.prices['ERROR'] += '\n\\subsubsection{{ERREUR : {}}}\\\\ \n'.format(region)
                print("ERROR REGION {}".format(region))
                continue
            # get list of wines from a region
            wines_region = df[(df['region'] == region)]
            sub_regions_df = wines_region.sub_region.unique()
            sub_regions_ref = []
            try:
                sub_regions_ref = c.ORDERED_SUB_REGIONS[region]
            except KeyError:
                # Error in region name (region mal orthograhiée)
                ordered_sub_regions = c.ORDERED_SUB_REGIONS
                msg = "{} {} \\\\\n".format(region, region_full.replace(' ', '_'))
                self.prices['ERROR'] += msg
                msg = "{}".format(region)
                # log_msg += msg + '\n'
            try:
                sub_ref = c.ORDERED_SUB_REGIONS[region]
            except KeyError:
                # Temporary test
                # s1 = set(region_full)
                # s2 = set('Test_string')
                msg = "{} {} \\\\\n".format(region, region.replace(' ', '_'), )
                self.prices['ERROR'] += msg
                msg = "{}".format(region)
                log_msg += msg + '\n'
                error_nbr += 1
                self.logger.error(msg)
                continue
            for sub_region in sub_regions_ref:
                sub_region_dflist = sub_regions_df.tolist()
                if self.check_presence(sub_region, sub_regions_ref):
                    if not self.check_presence(sub_region, sub_regions_ref):
                        msg = 'Erreur nom sous-région {} \n'.format(sub_region)
                        self.prices['ERROR'] += '\\noindent' + msg + '\\\\'
                        log_msg += msg + '\n'
                        error_nbr += 1

                    msg = "Subregions for region {}".format(sub_region)
                    self.logger.debug(msg)
                    # log_msg += msg + '\n'
                    # Empty sub_regions_df = NO
                    if sub_region != 'NO':
                        # self.prices['tmp'] += '\n\subsection{{{}}}\n'.format(sub_region)
                        pass
                    # get list of wines from a sub_region
                    wines_sub_region = wines_region[(wines_region['sub_region'] == sub_region)]
                    # Get the list of seller in this region
                    sellers = wines_sub_region.seller_name.unique()
                    for seller in sellers:
                        # get the wines from a given producer
                        msg = "Wines from producer {}".format(seller)
                        self.logger.debug(msg)
                        # log_msg += msg + '\n'

                        try:
                            # FIXME BUG if new seller, he has no description and it fails.
                            # FIXME if "fiche de degustation", it fails too even if the description is not used
                            # NEED to produce a description: "Seller name: NO description in red"
                            description = seller_dict[seller]['description'] + '\n'
                            presentation = seller_dict[seller]['presentation'] + '\n'

                            self.prices['tmp'] += f"\\needspace{{{need_space_wine}\\baselineskip}}"

                            if tasting_notes:
                                # seller += f"\#{idx} " + seller
                                self.prices[
                                    'tmp'] += f"\n {self.page(idx)}" + f"\n\\subsubsection{{{self.debug(idx)}{self.latex_cleaner(seller)}}}\n" + '\n'
                                idx += 1
                            else:
                                self.prices[
                                    'tmp'] += f"\n {self.page(idx)}" + f"\n\\subsubsection{{{self.debug(idx)}{self.latex_cleaner(description)}}}\n" + '\n'
                                idx += 1
                                self.prices[
                                    'tmp'] += f"\n {self.page(idx)}" + f"\n \\noindent\\desc{{{self.debug(idx)}{self.latex_cleaner(presentation)}}} \n" + '\n'
                                idx += 1
                            if len(presentation) > 2:
                                presentation += ' \\\\ \\bigskip'
                        except KeyError:
                            self.prices['ERROR'] += '\nPas de description pour {} \\\\'.format(
                                self.latex_cleaner(seller))
                            self.prices[
                                'tmp'] += f"\n {self.page(idx)}" + f"\n\\subsubsection{{{self.debug(idx)}{self.latex_cleaner(seller)}}}\n"
                            idx += 1
                            error_msg = "No description for {}".format(seller)
                            self.logger.error(error_msg)
                            msg = "Pas de description pour {}".format(seller)
                            log_msg += msg + '\n'
                            error_nbr += 1

                        # Get the wines sold by a producer
                        wines_seller = wines_sub_region[(wines_sub_region['seller_name'] == seller)]

                        # Get the list of color in this collection
                        colors = wines_seller.color.unique()

                        for color in colors:
                            # get the wines of a given color
                            msg = "Wines of color {}".format(color)
                            self.logger.debug(msg)
                            # log_msg += msg + '\n'
                            # Empty  color = NO
                            wines_tab = wines_seller[(wines_seller['color'] == color)]
                            # local sort here to apply custom filter
                            order_content = list(wines_tab.ordre.unique())
                            if order_content != [0]:
                                wines_tab = self.order_wine_sorter(wines_tab)
                            if color != 'NO':
                                if wines_tab.shape[0] < 2:
                                    adj = "L'" if color in c.APOSTROPHE_COLOR else 'Le '
                                    color_msg = f"{adj}{color}"
                                else:
                                    color_msg = "Les {}s".format(color)
                                if color in c.ALWAYS_MULTIPLES:
                                    color_msg = f'Les {color}'
                                try:
                                    color_msg = f"\n {self.page(idx)}" + f"{self.debug(idx)} " + color_msg
                                    self.prices[color.lower()] += '\\needspace{{{}\\baselineskip}}'.format(
                                        need_space_wine) + '\n\\subsubsubsection{{{}}}\n'.format(color_msg)
                                    idx += 1
                                except KeyError:
                                    msg = "Error couleur inconnue {}".format(color)
                                    self.logger.error(msg)
                                    log_msg += msg + '\n'
                                    self.prices['ERROR'] += msg
                                    error_nbr += 1

                            data_tab = ''
                            for index, wine in wines_tab.iterrows():
                                code = wine['code']
                                description = wine['description']
                                description = self.latex_cleaner(description)
                                name = wine['name']  # .replace("&", "\\&").replace("%", '\\%')
                                name = self.latex_cleaner(name)
                                price = wine['price']
                                vintage = wine['vintage']
                                note = wine['note']  # .replace("&", "\\&").replace("%", '\\%')
                                note = self.latex_cleaner(note)
                                # note = note.replace("Nouveauté", "")
                                if 'NM' in code:
                                    vintage = ''
                                if not 'Ne pas reprendre au tarif---'.lower() in note.lower():
                                    note = note.replace('recyclé', '')

                                    if "bio" not in note.lower():
                                        line = self.table_line.format(code=code, name=name, vintage=vintage, note=note,
                                                                      price=price)
                                    else:
                                        note = note.replace("BIO", "")
                                        if "en conversion" not in note.lower():
                                            line = self.table_bioline.format(code=code, name=name, vintage=vintage,
                                                                             note=note, price=price)
                                        else:
                                            line = self.table_line.format(code=code, name=name, vintage=vintage,
                                                                          note=note,
                                                                          price=price)
                                    if 'NO' not in description:
                                        line += self.noteline.format(d=description)
                                        # line += plop
                                    data_tab += line
                                self.wine_counter += 1
                                self.added_wines.append(code)

                            table = self.table.replace('--DATA--', data_tab)
                            # table.replace('\\\\\n\\end', '\n\\end')
                            try:
                                self.prices[color.lower()] += table
                            except KeyError:
                                msg = "Error couleur inconnue {}".format(color)
                                self.logger.error(msg)
                                log_msg += msg + '\n'
                                error_nbr += 1

                        self.prices['tmp'] = self.prices['tmp'] + self.prices['blanc'] + self.prices['rosé'] \
                                             + self.prices['rouge'] + self.prices['ambré'] \
                                             + self.prices['grenat'] + self.prices['bulles'] + self.prices['orange'] \
                                             + self.prices['no']
                        self.prices[region] += self.prices['tmp']
                        self.prices['tmp'] = ''
                        self.prices['rouge'] = ''
                        self.prices['blanc'] = ''
                        self.prices['ambré'] = ''
                        self.prices['grenat'] = ''
                        self.prices['rosé'] = ''
                        self.prices['no'] = ''
                        self.prices['bulles'] = ''
                        self.prices['orange'] = ''
                else:
                    msg = 'Sous-région {} non trouvée dans {} pour région {}!\\\\ \n'.format(
                        sub_region, sub_regions_df, region)
                    self.prices['ERROR'] += msg
                    log_msg += msg + '\n'
                    error_nbr += 1

            # region = ''

        return flat_list, log_msg, error_nbr

    def global_wine_sorter(self, list_of_wines):
        name_key = 'seller_name'
        color_key = 'color'
        # price_key = 'price'
        region_key = 'region'
        code_key = 'code'
        sub_region_key = 'sub_region'
        # From here: https://stackoverflow.com/questions/1143671/python-sorting-list-of-dictionaries-by-multiple-keys
        # reverse with  -d
        newlist = sorted(list_of_wines,
                         key=lambda d: (d[region_key], d[sub_region_key], d[name_key], d[color_key], d[code_key]))
        return newlist

    def order_wine_sorter(self, list_of_wines):
        order_key = 'ordre'
        # From here: https://stackoverflow.com/questions/1143671/python-sorting-list-of-dictionaries-by-multiple-keys
        # reverse with  -d
        df = list_of_wines.sort_values(by=[order_key], ascending=True)
        return df

    def research_wines(self, d=[], keys=[], data=[]):
        df = pd.DataFrame(d)
        query = df[(df[keys[0]] == data[0]) & (df[keys[1]] == data[1])]
        return query

    def order_list_old(self, list_data, model):
        return_list = []
        for el in model:
            for l in list_data:
                if el.lower() in l.lower():
                    return_list.append(l)

        return return_list

    def check_presence(self, item, list_data):
        ret = False
        for x in list_data:
            if x == item:
                ret = True
                break
        return ret

    def latex_cleaner(self, bad_string):
        good_string = bad_string.replace("&", "\\&").replace("%", '\\%').replace('\n', '').replace('\r',
                                                                                                   '')  # .replace('oe','\\oe{}' )
        return good_string


# https://www.saltycrane.com/blog/2011/12/how-get-filename-and-its-parent-directory-python/
def get_file_with_parents(filepath, levels=1):
    common = filepath
    for i in range(levels + 1):
        common = os.path.dirname(common)
    return os.path.relpath(filepath, common)
