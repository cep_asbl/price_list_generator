#!/usr/bin/env python3
#  -*- coding: utf-8 -*-
#  # @author: Arnaud Joset
#
#  This file is part of Price_list_generator.
#
# Price_list_generator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Price_list_generator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Price_list_generator.  If not, see <http://www.gnu.org/licenses/>.
#

# REGION_NOT_FOUND = 'Region not found'
# REGIONS = ['Alsace', 'Bourgogne', 'Beaujolais', 'Loire', 'Rhône', 'Provence', REGION_NOT_FOUND]

PROBLEM = 'REGION MAL ORTHOGRAPHIÉE'

ORDERED_REGIONS = ['Champagne', 'Alsace', 'Jura', 'Bourgogne', 'Mâconnais', 'Beaujolais' , 'Rhône_Nord', 'Rhône_Sud',
                   'Provence', 'Corse', 'Languedoc', 'Roussillon', 'Bordelais', 'Sud_Ouest', 'Bordeaux', 'Loire', 'Belgique']

ORDERED_REGIONS += ['Table 1', 'Table 2', 'Table 3','Table 4','Table 5','Table 6','Table 7','Table 8','Table 9',
                    'Table 10','Table 11', 'Table 12', 'Table 13', 'Table 14', 'Table 15']

ALPHABET = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
            'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'NO']

ORDERED_SUB_REGIONS = {'Alsace': ALPHABET,
                       'Jura': ALPHABET,
                       'Belgique': ALPHABET,
                       'Loire': ALPHABET,
                       'Champagne': ALPHABET,
                       'Beaujolais': ALPHABET,
                       'Mâconnais': ALPHABET,
                       'Savoie': ALPHABET,
                       'Rhône_Nord': ALPHABET,
                       'Rhône_Sud': ALPHABET,
                       'Provence': ALPHABET,
                       'Corse':ALPHABET,
                       'Languedoc': ALPHABET,
                       'Roussillon': ALPHABET,
                       'Bourgogne': ALPHABET,
                       'Bordelais': ALPHABET,
                       'Sud_Ouest': ALPHABET,
                       'Bordeaux': ALPHABET,
                       'Table 1': ALPHABET,
                       'Table 2': ALPHABET,
                       'Table 3': ALPHABET,
                       'Table 4': ALPHABET,
                       'Table 5': ALPHABET,
                       'Table 6': ALPHABET,
                       'Table 7': ALPHABET,
                       'Table 8': ALPHABET,
                       'Table 9': ALPHABET,
                       'Table 10': ALPHABET,
                       'Table 11': ALPHABET,
                       'Table 12': ALPHABET,
                       'Table 13': ALPHABET,
                       'Table 14': ALPHABET,
                       'Table 15': ALPHABET,
                       }

ORDERED_COLOR = ['Blanc', 'Rosé', 'Rouge', 'Ambré', 'Orange', 'Grenat', 'Bulles', 'NO']
APOSTROPHE_COLOR = ['Ambré', 'Orange', 'ambré', 'orange']
ALWAYS_MULTIPLES = ['Bulles', 'bulles']

