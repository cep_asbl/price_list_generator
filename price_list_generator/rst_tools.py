#!/usr/bin/env python3
#  -*- coding: utf-8 -*-
#  # @author: Arnaud Joset
#
#  This file is part of Price_list_generator.
#
# Price_list_generator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Price_list_generator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Price_list_generator.  If not, see <http://www.gnu.org/licenses/>.
#

# FROM # https://stackoverflow.com/questions/11347505/what-are-some-approaches-to-outputting-a-python-data-structure-to-restructuredte

# import pandas as pd


def make_table(grid):
    # print(grid)
    #headers = ['code', 'name', 'vintage', 'note', 'price']
    #df = pd.DataFrame(grid, columns=headers)
    rst = ''
    rst_tab = ''
    # We use fixed column size
    sizes = [15, 40, 8, 27, 7]
    rst += create_frame(sizes)
    for line in grid:
        rst_tab += create_line(line, sizes)
    rst += rst_tab
    rst +=  create_frame(sizes) + '\n\n'
    return rst


def create_line(line, sizes):
    idx = 0
    rst = ''
    remaining_str = None
    sizes_tmp = sizes.copy()
    if len(line) == 1:
        # Just one line + the 4 |. We do not count the externals borns (+)
        sizes_tmp[0] = sum(sizes) + 4
    for item in line:
        if len(item) < sizes_tmp[idx]:
            # Do not need to cut the line
            rst += '|' + item + ' ' * (sizes_tmp[idx] - (len(item)))
        else:
            cut_items = split_long_str('', item, sizes_tmp[idx] - 20)
            cut_items = cut_items.split('\n')
            a = len(cut_items[0])
            new_item = cut_items[0].strip()
            rst += '|' + new_item + ' ' * (sizes_tmp[idx] - (len(new_item)))
            remaining_str = {'pos': idx, 'list': cut_items[1:]}
        idx += 1
    rst += '|\n'
    if remaining_str:
        for multiline_item in remaining_str['list']:
            a = len(multiline_item)
            multiline_item = multiline_item.strip()
            if len(line) == 1:
                line = ['']
            else:
                line = [''] * len(sizes_tmp)
            line[remaining_str['pos']] = multiline_item
            rst += create_line(line, sizes)
    return rst



def create_frame(sizes):
    line = '+'
    for s in sizes:
        line += '-' * s
        line += '+'
    line += '\n'
    return line


def split_long_str(good_str, my_str, n_cut):
    """
    Cut long lines after n_cut caracter. Find the position of the nearest space at n_cut position and
    insert an \n character. The function call herself recoursively to cut each segment longueur than cut.
    """
    if len(my_str) > n_cut:
        pos = my_str.find(' ', n_cut)
        good_str += my_str[:pos] + '\n'
        tail = my_str[pos:]
        return split_long_str(good_str, tail, n_cut)
    else:
        good_str += my_str
        return good_str


if __name__ == '__main__':
    my_str = """C'est une super longue ligne ha ha ha. Délicieux bouquet floral et épicé, serti de tanins fermes \
    en filigrane. Ce rouge se révèlera davantage dans cinq ans." (RVF juin 2018) Mais vraiment longues !!!"""
    str_cut = split_long_str('', my_str, 60)
    print(str_cut)
