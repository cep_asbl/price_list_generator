#!/usr/bin/env python
# Copyright (c) 2018 errol project
# This code is distributed under the GPLv3 License
# The code is inspired from https://github.com/rbarrois/aionotify

from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

try:
    import pypandoc
    long_description = pypandoc.convert(path.join(here, 'README.md'), 'rst')
except(IOError, ImportError):
    long_description = open(path.join(here, 'README.md'), encoding='utf-8').read()


# Arguments marked as "Required" below must be included for upload to PyPI.
# Fields marked as "Optional" may be commented out.

setup(
    name='price_list_generator',  # Required
    version='1.1.2',  # Required
    description='Price_list_generator is a program that takes a xlsx file (list of prices) as an input and generate a LaTeX or RST file.',
    long_description=long_description,  # Optional
    url='https://agayon.be',  # Optional
    author='Arnaud Joset',  # Optional
    author_email='info@agayon.be',
    classifiers=[
        # https://pypi.python.org/pypi?%3Aaction=list_classifiers
        'Development Status :: 3 - Alpha',
        'Environment :: Console',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Programming Language :: Python :: 3 :: Only',
        'Operating System :: POSIX :: Linux',
    ],
    packages=find_packages(exclude=[]),
    install_requires=[''],
    python_requires='>=3.5',
)
