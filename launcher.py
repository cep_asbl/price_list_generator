#!/usr/bin/env python3
#  -*- coding: utf-8 -*-
#  # @author: Arnaud Joset
#
#  This file is part of Price_list_generator.
#
# Price_list_generator is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Price_list_generator is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Price_list_generator.  If not, see <http://www.gnu.org/licenses/>.
#

import logging

from price_list_generator import table_reader
from price_list_generator import latex_gen
from price_list_generator import rst_gen

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
ch = logging.StreamHandler()
fh = logging.FileHandler('tarif.log', mode='w')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    handlers=(ch, fh)
                    )


def parameters():
    params = {'price_list': './data/liste_prix.xls',
              'debug': 'DEBUG',
              'type': 'latex',  # rst or LaTeX
              'breaks': 'never'  # manual, always, never
              }
    return params


def launch(parameters_dict=None):
    logger = logging.getLogger('price_list_generator')
    logger.info("Start launcher")
    log_msg = ''
    error_gen = 0
    error_prices = 0
    error_nbr = 0
    params = parameters()
    tr = table_reader.Table()
    # read table
    price_list, flat_list, log_msg_wine_reader, error_reader = tr.wines_reader(parameters=params)
    if price_list and flat_list:
        sellers_dict = tr.seller_reader(filename=params['price_list'])
        # if not parameters_dict:
        #     parameters_dict = tr.parameters_reader(filename=params['price_list'])
        regions_dict = tr.regions_reader(filename=params['price_list'])
        if params['type'].lower() == 'latex':
            # Generate latex files
            latex = latex_gen.Latex()
            # create tex tile
            ret = latex.gen_templates()
            if ret:
                error_nbr += 1
                log_msg += ret
                return log_msg, error_nbr
            flat_list, log_msg_gen_wines, error_gen = latex.generate_wines_list(flat_list, sellers_dict,
                                                                                parameters_dict, regions_dict)
            log_msg_prices, error_prices = latex.write_prices()
            # compile tex file
            latex.pdflatex()
            log_msg += log_msg_wine_reader + log_msg_gen_wines + log_msg_prices
        elif params['type'].lower() == 'rst':
            rst_instance = rst_gen.Rst()
            rst_instance.gen_templates()
            rst_instance.generate_wines_list(flat_list, sellers_dict, parameters_dict, regions_dict)
            rst_instance.write_prices()
            rst_instance.rst2pdf()
    else:
        logger.critical("Check your Excel File")
    error_nbr = error_reader + error_gen + error_prices
    print(log_msg)
    print(f"Nombre erreurs : {error_nbr}")

    return log_msg, error_nbr


if __name__ == '__main__':
    parameters_dict = {'tasting_notes': '', 'breaks': 'never', 'breaks_list': ["91","110",25, 18], 'debug_breaks': True}
    parameters_dict = {'tasting_notes': '', 'breaks': 'never', 'breaks_list': ["91", "110", 25, 18],
                       'debug_breaks': True}
    # breaks: manual, always, never
    launch(parameters_dict)
